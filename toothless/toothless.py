#this is going to be our toothless game
import random

# Window
WIDTH = 1600
HEIGHT = 800

# Main Guy
toothless = Actor('toothless_small')
toothless.topleft = (WIDTH/2), (HEIGHT/2 + 100)
toothless_direction = 'right'
toothless_fire = True
toothless_gravity = 3

# Stormfly
stormfly = Actor('stormfly_small')
stormfly.topleft = (100), (200)
stormfly_direction = 'right'
stormfly_fire = True
stormfly_gravity = 3

# Flappy Birds
bird = Actor('bird0')
bird.topleft = (-500), (300)
bird_left = Actor('bird0_left')
bird_left.topleft = (-100), (200)
birddead = False



# Fireball
fireballs = []



def draw():
    """This puts the assets on the screen"""
    screen.blit('background.jpg', (0,0))
    toothless.draw()
    stormfly.draw()
    bird.draw()
    bird_left.draw()
    if len(fireballs) > 0:
        for fireball in fireballs:
            fireball['actor'].draw()


def update():
    """This is the event loop that makes the game play"""
    toothless_keys()
    stormfly_keys()
    if toothless.colliderect(stormfly):
        toothless_hurt()
        stormfly_hurt()
    bird_drop()
    bird_flap()
    if toothless.colliderect(bird) or toothless.colliderect(bird_left):
        toothless_hurt()
    if stormfly.colliderect(bird) or stormfly.colliderect(bird_left):
        stormfly_hurt()
    if len(fireballs) > 0:
        for index, fireball in enumerate(fireballs):
            move_fireball(fireball)
            if fireball['actor'].colliderect(bird):
                bird.image = 'birddead'
                birddead = True
                fireball_impact(fireball['actor'])
                del fireballs[index]
    


def generate_fireball(x, y, direction):
    """make a fireball"""
    fireball = {}
    fireball['actor'] = Actor('fireball2')
    fireball['actor'].topleft = (x), (y)
    fireball['direction'] = direction
    return fireball

def move_fireball(fireball):
    """move the fireball in the appropriate direction"""
    if fireball['direction'] == 'right':
        fireball['actor'].angle = 0
        fireball['actor'].x += 5
    if fireball['direction'] == 'left':
        fireball['actor'].angle = 180
        fireball['actor'].x -= 5
    if fireball['direction'] == 'upright':
        fireball['actor'].angle = 45
        fireball['actor'].x += 5
        fireball['actor'].y -= 5
    if fireball['direction'] == 'upleft':
        fireball['actor'].angle = 135
        fireball['actor'].x -= 5
        fireball['actor'].y -= 5
    if fireball['direction'] == 'downright':
        fireball['actor'].angle = -45
        fireball['actor'].x += 5
        fireball['actor'].y += 5
    if fireball['direction'] == 'downleft':
        fireball['actor'].angle = -135
        fireball['actor'].x -= 5
        fireball['actor'].y += 5

def destroy_fireball(idx):
    """remove fireball"""
    def destroy():
        global fireballs
        del fireballs[idx]
    return destroy

def fireball_impact(fireball):
    fireball.image = 'explosion2'

def stormfly_keys():
    """We will move stormfly around using the WASD keys"""
    global stormfly, stormfly_direction, fireballs, stormfly_fire, stormfly_gravity
    if stormfly.y < HEIGHT +20:
        stormfly.y += stormfly_gravity
    if keyboard.A:
        stormfly.angle = 0
        if stormfly.image == 'stormfly_small':
            stormfly.image = 'stormfly_small-left'
        if stormfly.x > 40:
            stormfly.x -= 5
        else:
            #stormfly.x = WIDTH
            pass
        stormfly_direction = 'left'
    if keyboard.D:
        stormfly.angle = 0
        if stormfly.image == 'stormfly_small-left':
            stormfly.image = 'stormfly_small'
        if stormfly.x < WIDTH:
            stormfly.x = stormfly.x + 5
        else:
            pass
        stormfly_direction = 'right'
    if keyboard.W:
        if stormfly.image == 'stormfly_small':
            stormfly.angle = 45
            stormfly_direction = 'upright'
        elif stormfly.image == 'stormfly_small-left':
            stormfly.angle = -45
            stormfly_direction = 'upleft'
        if stormfly.y > 0:
            stormfly.y = stormfly.y - 10
        else:
            pass
    if keyboard.S:
        if stormfly.image == 'stormfly_small':
            stormfly.angle = -45
            stormfly_direction = 'downright'
        elif stormfly.image == 'stormfly_small-left':
            stormfly.angle = 45
            stormfly_direction = 'downleft'
        if stormfly.y < HEIGHT:
            stormfly.y = stormfly.y + 5
    if keyboard.Q and stormfly_fire:
        fireballs.append(generate_fireball(stormfly.x, stormfly.y, stormfly_direction))
        stormfly_fire = False
        clock.schedule_unique(stormfly_reset, 1.25)

def toothless_keys():
    """Moves toothless around the screen with keys"""
    global toothless, fireballs, toothless_direction, toothless_fire, toothless_gravity
    if toothless.y < HEIGHT + 20:
        toothless.y += toothless_gravity
    if keyboard.left:
        toothless.angle = 0
        if toothless.image == 'alien_hurt':
            toothless.image = 'alien_hurt-left'
        if toothless.image == 'toothless_small':
            toothless.image = 'toothless_small-left'
        if toothless.x > 40:
            toothless.x -= 5
        else:
            #toothless.x = WIDTH
            pass
        toothless_direction = 'left'
    if keyboard.right:
        toothless.angle = 0
        if toothless.image == 'alien_hurt-left':
            toothless.image = 'alien_hurt'
        if toothless.image == 'toothless_small-left':
            toothless.image = 'toothless_small'
        if toothless.x < WIDTH:
            toothless.x += 5
        else:
            #toothless.x = 0
            pass
        toothless_direction = 'right'
    if keyboard.up:
        if toothless.image == 'toothless_small':
            toothless.angle = 45
            toothless_direction = 'upright'
        elif toothless.image == 'toothless_small-left':
            toothless.angle = -45
            toothless_direction = 'upleft'
        if toothless.y > 20:
            toothless.y -= 10
        else:
            #toothless.y = HEIGHT
            pass
    if keyboard.down:
        if toothless.image == 'toothless_small':
            toothless.angle = -45
            toothless_direction = 'downright'
        elif toothless.image == 'toothless_small-left':
            toothless.angle = 45
            toothless_direction = 'downleft'
        if toothless.y < HEIGHT:
            toothless.y += 5
        else:
            #toothless.y = 0
            pass
    if keyboard.space and toothless_fire:
        fireballs.append(generate_fireball(toothless.x, toothless.y, toothless_direction))
        toothless_fire = False
        clock.schedule_unique(toothless_reset, 1.25)
        
def toothless_reset():
    global toothless_fire
    toothless_fire = True

def stormfly_reset():
    global stormfly_fire
    stormfly_fire = True

def stormfly_hurt():
    global stormfly_gravity
    stormfly.image = 'explosion2'
    sounds.eep.play()
    clock.schedule_unique(stormfly_heal, 0.25)
    stormfly_gravity = 12
 
def stormfly_heal():
    global stormfly_gravity
    stormfly.image = 'stormfly_small'
    stormfly_gravity = 3

def toothless_hurt():
    global toothless_gravity
    toothless.image = 'explosion2'
    sounds.eep.play()
    clock.schedule_unique(toothless_heal, 0.25)
    toothless_gravity = 12
    
def toothless_heal():
    global toothless_gravity
    toothless.image = 'toothless_small'
    toothless_gravity = 3
    
def bird_drop():
    """drops birds in random places"""
    global bird
    if bird.y < (HEIGHT + 30):
        bird.y = bird.y + 5
        bird.x = bird.x + 3
        if bird.image == 'birddead':
            bird.y = bird.y + 10
    else:
        bird.y = -100
        bird.x = random.randint(10, (WIDTH - 10))
        bird.image = 'bird0'
        bird.dead = False
    if bird_left.y < (HEIGHT + 30):
        bird_left.y = bird_left.y + 5
        bird_left.x = bird_left.x - 3
    else:
        bird_left.y = -100
        bird_left.x = random.randint(10, (WIDTH - 10))
        
def bird_flap():
    if bird.image == 'bird0':
        bird.image = 'bird1'
    elif bird.image == 'bird1':
        bird.image = 'bird2'
    elif bird.image == 'bird2':
        bird.image = 'bird0'
    if bird_left.image == 'bird0_left':
        bird_left.image = 'bird1_left'
    elif bird_left.image == 'bird1_left':
        bird_left.image = 'bird2_left'
    elif bird_left.image == 'bird2_left':
        bird_left.image = 'bird0_left'
