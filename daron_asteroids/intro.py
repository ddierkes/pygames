import random


# Window
WIDTH = 1600
HEIGHT = 800

# Main Guy
alien = Actor('alien')
alien.topleft = (WIDTH/2), (HEIGHT/2 + 100)

# Bad Guys
asteroid = Actor('asteroid', center=(300, 300))
asteroid.topleft = (WIDTH - 40), (HEIGHT - 40)
asteroid2 = Actor('asteroid', center=(300, 300))
asteroid2.topleft = (WIDTH - 70), (HEIGHT - 20)
asteroid3 = Actor('asteroid', center=(300, 300))
asteroid3.topleft = (WIDTH - 50), (HEIGHT - 70)

# Global Variables
direction = 'nw'
direction2 = 'se'
direction3 = 'sw'
hearts = 15
message = ""
recovery = True
dead = False
player2 = False

# Initializations
over_box = Rect(((WIDTH/2 - WIDTH/6), HEIGHT/3), (WIDTH/3, HEIGHT/4))
life_box = Rect((10, 20), (100, 40))
music.play('starman')

def draw():
    """This puts the assets on the screen"""
    screen.blit('background.jpg', (0,0))
    alien.draw()
    asteroid.draw()
    asteroid2.draw()
    asteroid3.draw()
    screen.draw.textbox(str(hearts) + " POINTS", life_box)
    screen.draw.textbox(message, over_box)
    

def update():
    """This is the event loop that makes the game play"""
    global recovery, player2
    if recovery:
        recover_life()
    if alien.colliderect(asteroid):
        set_alien_hurt()
        set_direction(1)
    if alien.colliderect(asteroid2):
        set_alien_hurt()
        set_direction(2)
    if alien.colliderect(asteroid3):
        set_alien_hurt()
        set_direction(3)
    move_asteroid()
    move_asteroid2()
    move_asteroid3()
    checkKeys()
    if keyboard.q:
        player2 = not player2

def recover_switch():
    """introduces a blocking mechanism to keep recovery moving slowly"""
    global recovery
    recovery = True

def recover_life():
    """a constant healing process"""
    global recovery, hearts, message
    recovery = False
    clock.schedule(recover_switch, 0.5)
    hearts = hearts + 1

 

def checkKeys():
    """Moves the alien around the screen with keys"""
    global alien
    if keyboard.left:
        if alien.image == 'alien_hurt':
            alien.image = 'alien_hurt-left'
        if alien.image == 'alien':
            alien.image = 'alien-left'
        if alien.x > 40:
            alien.x -= 5
        else:
            alien.x = WIDTH
    if keyboard.right:
        if alien.image == 'alien_hurt-left':
            alien.image = 'alien_hurt'
        if alien.image == 'alien-left':
            alien.image = 'alien'
        if alien.x < WIDTH:
            alien.x += 5
        else:
            alien.x = 0
    if keyboard.up:
        if alien.y > 20:
            alien.y -= 5
        else:
            alien.y = HEIGHT
    if keyboard.down:
        if alien.y < HEIGHT:
            alien.y += 5
        else:
            alien.y = 0



def on_mouse_down(pos):
    """click on the alien and he will scream"""
    if alien.collidepoint(pos):
        set_alien_hurt()

def set_direction(num):
    """Sets a random direction for the asteroids"""
    global direction, direction2, direction3
    rNum = random.randint(0,3)
    if num == 1:
        current = direction
        direction = ["nw", "sw", "ne", "se"][rNum]
        if current == direction:
            set_direction(1)
    if num == 2:
        current = direction2
        direction2 = ["nw", "sw", "ne", "se"][rNum]
        if current == direction2:
            set_direction(2)
    if num == 3:
        current = direction3
        direction3 = ["nw", "sw", "ne", "se"][rNum]
        if current == direction3:
            set_direction(3)

def move_asteroid():
    """Moves the asteroid around"""
    asteroid.angle += 1
    if direction == "nw" or direction == "sw":
        if asteroid.x > 40:
            asteroid.x -= 5
        else:
            asteroid.x = WIDTH
    if direction == "ne" or direction == "se":
        if asteroid.x < WIDTH:
            asteroid.x += 5
        else:
            asteroid.x = 0
    if direction == "ne" or direction == "nw":
        if asteroid.y > 20:
            asteroid.y -= 5
        else:
            asteroid.y = HEIGHT
    if direction == "se" or direction == "sw":
        if asteroid.y < HEIGHT:
            asteroid.y += 5
        else:
            asteroid.y = 0

def move_asteroid2():
    """redundant code to consolidate later"""
    asteroid2.angle += 1
    if direction2 == "nw" or direction2 == "sw":
        if asteroid2.x > 40:
            asteroid2.x -= 5
        else:
            asteroid2.x = WIDTH
    if direction2 == "ne" or direction2 == "se":
        if asteroid2.x < WIDTH:
            asteroid2.x += 5
        else:
            asteroid2.x = 0
    if direction2 == "ne" or direction2 == "nw":
        if asteroid2.y > 20:
            asteroid2.y -= 5
        else:
            asteroid2.y = HEIGHT
    if direction2 == "se" or direction2 == "sw":
        if asteroid2.y < HEIGHT:
            asteroid2.y += 5
        else:
            asteroid2.y = 0

def move_asteroid3():
    """
    PLAYER2 is a rock!  Plus
    redundant code to consolidate later
    """
    asteroid3.angle += 1
    if not player2:
        if direction3 == "nw" or direction3 == "sw":
            if asteroid3.x > 40:
                asteroid3.x -= 5
            else:
                asteroid3.x = WIDTH
        if direction3 == "ne" or direction3 == "se":
            if asteroid3.x < WIDTH:
                asteroid3.x += 5
            else:
                asteroid3.x = 0
        if direction3 == "ne" or direction3 == "nw":
            if asteroid3.y > 20:
                asteroid3.y -= 5
            else:
                asteroid3.y = HEIGHT
        if direction3 == "se" or direction3 == "sw":
            if asteroid3.y < HEIGHT:
                asteroid3.y += 5
            else:
                asteroid3.y = 0
    if player2:
        if keyboard.a:
            if asteroid3.x > 40:
                asteroid3.x -= 5
            else:
                asteroid3.x = WIDTH
        if keyboard.d:
            if asteroid3.x < WIDTH:
                asteroid3.x += 5
            else:
                asteroid3.x = 0
        if keyboard.w:
            if asteroid3.y > 20:
                asteroid3.y -= 5
            else:
                asteroid3.y = HEIGHT
        if keyboard.s:
            if asteroid3.y < HEIGHT:
                asteroid3.y += 5
            else:
                asteroid3.y = 0

def set_alien_hurt():
    """makes the alien scream and wince"""
    global hearts
    if not dead:
        if alien.image == 'alien':
            alien.image = 'alien_hurt'
        if alien.image == 'alien-left':
            alien.image = 'alien_hurt-left'
        clock.schedule_unique(set_alien_normal, 0.5)
        if hearts > 0:
            sounds.eep.play()
            hearts = hearts - 1
        if hearts == 0:
            game_over()

def game_over():
    """Sets the dead session in place with a sad trombone"""
    global message, dead
    dead = True
    message = "YOU DIED"
    if alien.image == 'alien' or  alien.image == 'alien_hurt':
        alien.image = 'dead'
    if alien.image == 'alien-left' or alien.image == 'alien_hurt-left':
        alien.image = 'dead-left'
    clock.schedule_unique(play_sad, 1)
    clock.schedule_unique(game_on, 15)

def message_clear():
    """Removes the game on message on schedule"""
    global message, dead
    if not dead:
        message = ""

def play_sad():
    """to clear the damage taking sound"""
    sounds.sad.play()
    
def game_on():
    """Cheers for the new game and upright avatar"""
    global dead, message
    dead = False
    sounds.cheer.play()
    message = "STAY ALIVE"
    alien.image = 'alien'
    clock.schedule_unique(message_clear, 2)

def set_alien_normal():
    """restores the alien after he is hurt"""
    if alien.image == 'alien_hurt':
        alien.image = 'alien'
    if alien.image == 'alien_hurt-left':
        alien.image = 'alien-left'
